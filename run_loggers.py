from redis import Redis
from time import sleep


r = Redis()
r.publish('event_logger', 'START')
r.publish('logger', 'START')


sleep(2)
r.publish('logger', 'KILL')
r.publish('event_logger', 'KILL')


