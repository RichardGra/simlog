"""
Project: Cockpit Logging App
Author: Richard Granec
"""

# def read_config_file(config_values,data_to_log):
#     """
#     Parse all neccessary info from config.yaml
#     """
#     import yaml
#     import os.path
#     try:
#         # If config file is not founded -> genereate default
#         if not(os.path.isfile('../config.yaml')):
#             print("Config file not found ! \n Generate default config file.")
#             create_default_config()

#         with open('../config.yaml', 'r') as configFile:
#             config = yaml.load(configFile)

#             for key in config["log_variables"]:
#                 data_to_log.append(key)

#             for key in config["config_variables"]:
#                 config_values[key] = config["config_variables"][key]

#     except EnvironmentError as err:
#         print("Error: Cannot open or create config file ! \n" + err)

def read_configfile(name_of_key, store_key_variables):
    """
    Parse top lvl key from config file
    """
    import yaml
    import os.path
    try:
        # Config can be stored in general folder (../../) if it's shared
        # between simcom or logdotcom.
        # Otherwise config is stored in simLog folder (../).
        if (os.path.isfile('../../config.yaml')):
            config_path = '../../config.yaml'
        elif (os.path.isfile('../config.yaml')):
            config_path = '../config.yaml'
        else:
            print("Config file not found ! \n Generate default config file.")
            config_path = '../config.yaml'
            create_default_config(config_path)


        with open(config_path, 'r') as configFile:
            config = yaml.load(configFile)

            for key in config[name_of_key]:
                if type(store_key_variables) is list:
                    store_key_variables.append(key)
                elif type(store_key_variables) is dict:
                    store_key_variables[key] = config[name_of_key][key]

    except EnvironmentError as err:
        print("Error: Cannot open or create config file ! \n" + err)
    except AttributeError as err:
        print("Type of data from {} key is diffrent then {} \n {}"
            .format(name_of_key, type(store_key_variables), err))


def create_default_config(config_path):
    """
    If there is no config file, create default
    """
    try:
        with open(config_path,'w') as newConfigFile:
            defaultConfigFile =("""config_variables:
    redis_host: localhost
    redis_host_scenMan: localhost
    redis_port: 6379
    scen_path : C:\Git\simCom\scenarios
    is_default_config: True

log_variables:
    - altitude
    - heading
    - ver_speed
    - ias_speed

read_sim_variables:
    heading : {address: 0x580,  size: u, correction_const: 8.381903171539307e-8}
    altitude: {address: 0x6020, size: f, correction_const: 3.2808399}
    ver_speed: {address: 0x2c8, size: d, correction_const: 0.768946875}
    ias_speed: {address: 0x2bc, size: u, correction_const: 0.008990464453125}
    ispaused: {address: 0x264, size: b, correction_const: 1}

write_sim_variables:
    pause: {address: 0x0262,  size: b, correction_const: 1}

events_log_variables:
    take_off: {operand_A: altitude, operand_B: 4000, operator: ">"}
    landed: {operand_A: altitude, operand_B: 3000, operator: "<="}


#  - b: a 1-byte unsigned value, to be converted into a Python int
#  - c: a 1-byte signed value, to be converted into a Python int
#  - h: a 2-byte signed value, to be converted into a Python int
#  - H: a 2-byte unsigned value, to be converted into a Python int
#  - d: a 4-byte signed value, to be converted into a Python int
#  - u: a 4-byte unsigned value, to be converted into a Python long
#  - l: an 8-byte signed value, to be converted into a Python long
#  - L: an 8-byte unsigned value, to be converted into a Python long
#  - f: an 8-byte floating point value, to be converted into a Python double)

            newConfigFile.write(defaultConfigFile)"""
            )

    except EnvironmentError as err:
        print("ERROR: Cannot create default config file \n" + err)
