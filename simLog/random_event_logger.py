class RandomEventLogger:

    def __init__(self):
        from operator import gt, ge, eq, le, lt
        from logging_support import Connections

        self.values_to_log = ["event_name", "time_of_occurrance"]
        self.io_connector = Connections(self.values_to_log, "events_")
        self.random_events = []
        self._add_config_random_events()
        self._operators_table = {
            ">":  gt,
            ">=": ge,
            "=":  eq,
            "==": eq,
            "<=": le,
            "<":  lt,
        }
        self.stop_logging = False

        # self.log_event_process(10,1)

    def add_random_event(self, name_of_event, conditions, can_repeat):
        new_event = RandomEvent(name_of_event, can_repeat)
        for condition in conditions:
            new_event.add_condition(
                condition["operand_A"],
                condition["operand_B"],
                condition["operator"],
            )
        self.random_events.append(new_event)


    def log_event_process(self, log_time, log_interval):
        from time import time
        from logging_support import wait_to_next_interval

        print("Event logging...")

        log_start = time()

        events_to_log = []
        while True:
            log_interval_start = time()

            for event in self.random_events:
                if self._check_event_conditions(event):
                    self._event_occurrence(events_to_log, event)
                else:
                    event.occurrence_in_previous_check = False

            wait_to_next_interval(log_interval_start, log_interval)

            # Stop logging process if no endless mode
            if ((log_time != 0 and time() - log_start >= log_time) or
                self.stop_logging):
                break

        for event in events_to_log:
            self.io_connector.csv_writer.writerow(
                {


                    "event_name" : event.name,
                    "time_of_occurrance": event.time,
                }
            )

        self.close_logger()

        if (self.io_connector.config["cloud_log_enabled"] and
         not self.io_connector.config["cloud_log_realtime"]):
            self.io_connector.azzure_connection.upload_to_azure_table()


    def _event_occurrence(self, events_to_log, event):
        """
        Log event only if it's first occurrence
        """
        if event.can_repeat:
            # Event which can repeat
            # Log them only if their coditions were not occurred
            # in previous check
            if not event.occurrence_in_previous_check:
                self._log_occurred_event(events_to_log, event)
        else:
            # Events without repeating
            # Log them only if they were not logged yet
            if not event.occurrence:
                self._log_occurred_event(events_to_log, event)


    def close_logger(self):
        """
        Close all connections
        """
        self.stop_logging = True
        self.io_connector.close_connections()


    def _log_occurred_event(self, events_to_log, event):
        from datetime import datetime

        # Cut miliseconds to 3 decimal places
        time_of_occurrance = datetime.now().strftime('%H:%M:%S.%f')
        if len(time_of_occurrance) == 15:
            time_of_occurrance = time_of_occurrance[:-3]

        occurred_event = (OccurredEvent
            (
            event.name,
            time_of_occurrance
            )
        )
        events_to_log.append(occurred_event)

        event.occurrence = True
        event.occurrence_in_previous_check = True

        if (self.io_connector.config["cloud_log_enabled"] and
         self.io_connector.config["cloud_log_realtime"]):

            if not self.io_connector.azzure_connection.table_created:
                self.io_connector.azzure_connection.create_table_on_azure()

            self.io_connector.azzure_connection.upload_event_occurrence(occurred_event)

    def _check_event_conditions(self, event):
        """
        Check if some of log conditions is met
        """
        for condition in event.log_conditions:
            if not self._log_condition_occurred(condition):
                return False
        return True

    def _log_condition_occurred(self, condition):
        """
        Check given condition
        """
        operand_A = self._get_value(condition.operand_A)
        operand_B = self._get_value(condition.operand_B)
        return self._operators_table[condition.operator](operand_A, operand_B)

    def _get_value(self, value_to_covert):
        """
        Convert value from string.
        If value_to_covert is a name, it's get its value from database
        """
        try:
            value = float(value_to_covert)
            return value
        except ValueError:
            value =  float(self.io_connector.db_connection.read(value_to_covert))
            return value

    def _add_config_random_events(self):
        """
        Read and add random events defined in config file
        """
        from config_support import read_configfile

        events_from_config = {}
        read_configfile("events_log_variables", events_from_config)

        for event in events_from_config:
            conditions = self._get_all_conditions(events_from_config[event])
            self.add_random_event(event, conditions, False)

        repeating_events_from_config = {}
        read_configfile("repeating_events_log_variables", repeating_events_from_config)

        for event in repeating_events_from_config:
            conditions = self._get_all_conditions(repeating_events_from_config[event])
            self.add_random_event(event, conditions, True)



    def _get_all_conditions(self, event):
        """
        Get all conditions from configfile's record and create list of them
        """
        from sys import stderr

        num_of_elements_in_condition = 3
        list_of_conditions = []
        if len(event) == num_of_elements_in_condition:
            try:
                list_of_conditions.append({
                    "operand_A": event["operand_A"],
                    "operand_B": event["operand_B"],
                    "operator":  event["operator"],
                })
            except KeyError:
                list_of_conditions.append({
                    "operand_A": event["operand_A_0"],
                    "operand_B": event["operand_B_0"],
                    "operator":  event["operator_0"],
                })

        elif len(event) % num_of_elements_in_condition == 0:
            number_of_conditions = len(event) // num_of_elements_in_condition
            for i in range(0, number_of_conditions):
                list_of_conditions.append({
                    "operand_A": event["operand_A_" + str(i)],
                    "operand_B": event["operand_B_"+ str(i)],
                    "operator":  event["operator_"+ str(i)],
                })
        else:
            print("Wrong format of event log condition in config file", file=stderr)
            return []

        return list_of_conditions


class RandomEvent:
    def __init__(self,name_of_event, can_repeat):
        self.name = name_of_event
        self.log_conditions = []
        self.can_repeat = can_repeat
        self.occurrence = False
        self.occurrence_in_previous_check = False

    def add_condition(self, operand_A, operand_B, operator):
        self.log_conditions.append(LogCondition(operand_A, operand_B, operator))

class LogCondition:
    def __init__(self,opA,opB,op):
        self.operand_A = opA
        self.operand_B = opB
        self.operator = op

class OccurredEvent:
    def __init__(self, name, time):
        self.name = name
        self.time = time

RandomEventLogger()