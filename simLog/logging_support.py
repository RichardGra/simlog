class Connections:

    def __init__(self, fieldnames, aditional_name=""):
        from redis_connection import Redis_connection
        from datetime import datetime
        from cloud_connection import Azzure_connector
        from config_support import read_configfile

        self.config = {}
        read_configfile("config_variables", self.config)

        self._log_fieldnames = fieldnames
        self.log_data_name = (
            "log_"
            + aditional_name
            + self.config["project"]
            + "_" + datetime.utcnow().strftime('%Y-%m-%d_%H-%M')
        )
        self.csv_writer = self._csv_init()
        self.csv_writer.writeheader()
        self.db_connection = Redis_connection()

        if self.config["cloud_log_enabled"]:
            self.azzure_connection = Azzure_connector(
                self.log_data_name,
                self.config["azure_acc_name"],
                self.config["azure_acc_key"],
                self.config["project"],
            )

    def close_connections(self):
        """
        Close I/O object connections
        """
        self.csv_file.close()

    def upload_log_to_cloud(self):
        if self.config["table_log_enabled"]:
            self.azzure_connection.upload_to_azure_table()

    def generate_czml(self):
        from czml_gen import czml_gen

        log_folder = self.config["log_path"]
        if log_folder[-1] != "/" and log_folder[-1] != "\\":
            log_folder = log_folder + "/"

        path_to_czml = self.config["path_to_store_czml"]
        if path_to_czml[-1] != "/" and path_to_czml[-1] != "\\":
            path_to_czml = path_to_czml + "/"

        czml_gen(
            log_folder + self.log_data_name + ".csv",
            path_to_czml
        )

    def _csv_init(self):

        """
        Initialize CSV used for logging data
        """
        from csv import DictWriter

        log_folder = self.config["log_path"]
        if log_folder[-1] != "/" and log_folder[-1] != "\\":
            log_folder = log_folder + "/"

        self.csv_file = open( log_folder + self.log_data_name + ".csv", 'w', newline='')
        return DictWriter(self.csv_file, fieldnames=self._log_fieldnames)


def wait_to_next_interval(log_interval_start, log_interval):
    """
    Sleeps program until next log interval
    """
    from time import time, sleep

    elapsed = time() - log_interval_start
    time_to_next_interval = log_interval - elapsed
    sleep(time_to_next_interval)
