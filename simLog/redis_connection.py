class Redis_connection:

    def __init__(self):
        from redis import StrictRedis
        from config_support import read_configfile

        self.config = {}
        read_configfile("config_variables", self.config)
        self.connection = StrictRedis(
                host=self.config["redis_host"],
                port=self.config["redis_port"],
                charset="utf-8",
                db=0,
                decode_responses=True,
            )
        self._wait_for_redis_connection()

    def close_connection(self):
        self.connection.connection_pool.disconnect()


    def write(self, key, value):
        self.connection.set(key,value)

    def read(self, key):
        return self.connection.get(key)

    def _wait_for_redis_connection(self):
        from time import sleep

        for reconn_index in range (1,5):
            if not self._redis_connection_is_established():
                print("Cannot connect to Redis. Retrying after {} seconds ..."
                    .format(5 * reconn_index))
                sleep(5 * reconn_index)
            else:
                # print("Connected to Redis.")
                return
        print("\nDue problem with Redis connection is not possible to run simLog")
        exit(1)

    def _redis_connection_is_established(self):
        """
        Test connection to redis DB
        """
        from redis import exceptions

        try:
            self.connection.ping()
        except (exceptions.ConnectionError,
                exceptions.BusyLoadingError):
            return False
        return True