def czml_gen(csv_file_path, output_file_path = "./czml_from_csv.czml"):
    import csv

    positions = []
    with open (csv_file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        rows = [row for row in reader]
        for row in rows:
            positions.append(float(row["run_time"]))
            positions.append(float(row["longitude"]))
            positions.append(float(row["latitude"]))
            positions.append(float(row["altitude"]) / 3.2808399 )

    with open(output_file_path + "simflight.czml", "w") as czml_file:
        models= {
            "airbus320" : "../models/CesiumAir/320.glb",
            "cessna"    : "../models/CesiumAir/dr40.glb",
        }

        start = "2012-08-04T10:00:00Z"
        end = "2012-08-04T15:00:00Z"

        czml_data = {
            'start' : start ,
            'end' : str(end),
            'interval' : start + "/" + end,
            'current_time' : str(start),
            'multiplier' : 3,
            'model' : models["airbus320"],
            'positions' : positions
        }

        czml_content = """
    [
        {{
        "id" : "document",
        "name" : "CZML Path",
        "version" : "1.0",
        "clock": {{
            "interval": "{interval}",
            "currentTime": "{current_time}",
            "multiplier": {multiplier}
        }}
    }},
    {{
        "id" : "path",
        "name" : "Airplane from sim",
        "" : "<p>Hang gliding flight log data from Daniel H. Friedman.<br>Icon created by Larisa Skosyrska from the Noun Project</p>",
        "availability" : "2012-08-04T10:00:00Z/2012-08-04T15:00:00Z",
        "path" : {{
            "material" : {{
                "polylineOutline" : {{
                    "color" : {{
                        "rgba" : [255, 0, 255, 255]
                    }},
                    "outlineColor" : {{
                        "rgba" : [0, 255, 255, 255]
                    }},
                    "outlineWidth" : 5
                }}
            }},
            "width" : 8,
            "leadTime" : 0,
            "trailTime" : 1000,
            "resolution" : 5
        }},
        "label": {{
            "show":true,
            "text": "Sim aircraft",
            "horizontalOrigin": "LEFT",
            "pixelOffset": {{"cartesian2": [0,-100]}},
            "fillColor": {{"rgba": [255, 255, 255, 255]}}
        }},
        "model": {{
            "gltf" : "{model}",
            "scale": 2.0,
            "minimumPixelSize": 128
        }},

        "position" : {{
            "epoch" : "{start}",
            "cartographicDegrees" : {positions}

        }}
    }}
    ]
        """.format(**czml_data)

        czml_file.write(czml_content)