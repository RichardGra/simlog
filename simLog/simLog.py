"Tool for logging data from Redis server and store them to Cloud"

class Logger:

    def __init__(self):
        from logging_support import Connections

        self._data_to_log = []
        self._config_values = {}
        self._datalog = {}
        self._get_config_data(self._config_values,self._data_to_log)
        self._data_to_log.append("run_time")
        self.io_connector = Connections(self._data_to_log)
        self.stop_logging = False
        # Add default variables to user defined


    def log_data_process(self, log_interval, log_time):
        """
        Continiously log data every X second for Y seconds
        If log_time = 0 -> endless mode
        """
        from time import time
        from logging_support import wait_to_next_interval

        print("Logging...")
        log_start = time()
        while True:
            log_interval_start = time()
            self.log_data(time() - log_start)
            wait_to_next_interval(log_interval_start, log_interval)

            # Stop logging process if no endless mode
            if ((log_time != 0 and time() - log_start >= log_time) or
                self.stop_logging):
                break

        self.close_logger()
        if self._config_values["cloud_log_enabled"]:
            self.io_connector.upload_log_to_cloud()

        if self._config_values["generate_czml_enabled"]:
            self.io_connector.generate_czml()

    def close_logger(self):
        """
        Close all connections
        """
        self.stop_logging = True
        self.io_connector.close_connections()

    def log_data(self,run_time):
        """
        Log all choosen data
        """
        self._datalog = self._read_all()
        self._datalog["run_time"] = run_time
        self.io_connector.csv_writer.writerow(self._datalog)

    def _get_config_data(self,config_values,data_to_log):
        """
        Read config file
        """
        from config_support import read_configfile

        read_configfile("config_variables", config_values)
        read_configfile("log_variables", data_to_log)

    def _read(self,name_of_record):
        """
            Read value to log
        """
        return self.io_connector.db_connection.read(name_of_record)

    def _read_all(self):
        """
        Read all variables defined by data_to_log list
        """
        return{
            name : self._read(name)
            for name in self._data_to_log
        }
