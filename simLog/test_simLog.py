from unittest import TestCase as _TestCase

class test_simLog(_TestCase):
    """
    Test for simLog
    """

    def test_config_read(self):
        import simLog
        testLogger = simLog.Logger()

        self.assertEqual(testLogger._data_to_log,[
            "log_time", "altitude", "heading", "ver_speed", "ias_speed", "time"])

        self.assertEqual()