from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
from time import sleep
import csv


# Connection to azure Cosmos DB
# table_service = TableService(connection_string='DefaultEndpointsProtocol=https;AccountName=simlog;AccountKey=TX8Dh6HeB20E3kPRiCu7XGkG1jfWXUNy1BD/XUlXO2UJQmrRu414x1ESU1mvYcY2gxNDB4bGbNtS0I6hKqsuwQ==;EndpointSuffix=core.windows.net')


class Azzure_connector:
    def __init__(self, log_name, acc_name, acc_key, project_name):
        self.log_name = log_name
        self.azure_name = log_name.replace("-","").replace("_","")
        self.table_service = TableService(
            account_name= acc_name,
            account_key= acc_key,
        )
        self.project_name = project_name
        self.table_created = False
        self._logged_events = 0

    def create_table_on_azure(self):
        self.table_service.create_table(self.azure_name)
        self.table_created = True

    def upload_to_azure_table(self):
        print("Uploading log to azure...")

        self.create_table_on_azure()
        with open( self.log_name + '.csv','r') as logFile:
            reader = csv.DictReader(logFile)
            rows = [row for row in reader]
            for row in rows:
                index = rows.index(row)
                row['RowKey'] = '{:04d}'.format(index)
                row['PartitionKey'] = self.project_name
                self.table_service.insert_entity(self.azure_name, row)
            print("Uploading: Done")

    def upload_event_occurrence(self, event):
        row  =  {
            "event_name" : event.name,
            "time_of_occureance" : event.time,
            "RowKey" : '{:04d}'.format(self._logged_events),
            "PartitionKey" : self.project_name,
        }
        self._logged_events = self._logged_events + 1
        self.table_service.insert_entity(self.azure_name, row)
