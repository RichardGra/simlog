from threading import Thread
from redis import Redis

class Listener(Thread):
    from sys import exit

    def __init__(self, redis, channels):
        Thread.__init__(self)
        self.redis = redis
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)


    def _process_logger_message(self, msg_data):
        if msg_data == "KILL".encode('utf-8'):
            self.pubsub.unsubscribe()
            print (self, "unsubscribed and finished")
            return True
        elif msg_data == "START".encode('utf-8'):
            self._run_logger()
            return False


    def _process_event_logger_message(self, msg_data):
        if msg_data == "KILL".encode('utf-8'):
            self.pubsub.unsubscribe()
            print (self, "unsubscribed and finished")
            return True
        elif msg_data == "START".encode('utf-8'):
            self._run_event_logger()
            return False

    def _run_logger(self):
        from simLog import Logger
        # print (item['channel'], ":", item['data'])

        print("Start logging")
        logger = Logger()
        logger.log_data_process(0.1,10)
        logger.close_logger()
        print("Stop logging")

    def _run_event_logger(self):
        from random_event_logger import RandomEventLogger
        # print (item['channel'], ":", item['data'])

        print("Start event logging")
        event_logger = RandomEventLogger()
        event_logger.log_event_process(0.1,10)
        event_logger.close_logger()
        print("Stop event logging")


    def run(self):
        for item in self.pubsub.listen():
            if item['channel'] == "logger".encode('utf-8'):
                if self._process_logger_message(item['data']):
                    break
            elif item['channel'] == "event_logger".encode('utf-8'):
                if self._process_event_logger_message(item['data']):
                    break

if __name__ == "__main__":
    redis = Redis()
    client_logger = Listener(redis, ['logger'])
    client_event_logger = Listener(redis, ['event_logger'])

    client_logger.start()
    client_event_logger.start()



