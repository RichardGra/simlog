import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="simLog",
    version="0.0.1",
    author="Richard Granec",
    description="App for logging data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/RichardGra/simlog",
    packages=setuptools.find_packages(),
    install_requires=[
          'redis',
          'pyyaml',
      ],
    extras_require={
        # For testing.
        'test': ['coverage'],
        },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)